cmake_minimum_required(VERSION 3.2 FATAL_ERROR)
project(editor VERSION 0.1 LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 11)

find_package(Curses REQUIRED)
include_directories(${CURSES_INCLUDE_DIR})

include_directories(include)

add_executable(app src/main.cpp src/EditorWindow.cpp
			src/TextInstance.cpp src/config.cpp)
target_link_libraries(app ${CURSES_LIBRARIES})

