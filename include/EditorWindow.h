#ifndef EDITOR_WINDOW_H
#define EDITOR_WINDOW_H

#include <vector>
#include <string>
#include <curses.h>

#include "config.h"
#include "TextInstance.h"

class EditorWindow
{
public:
	EditorWindow(int height, int width, int startx, int starty,
		std::string filename="");
	~EditorWindow();

	inline void writeChar(char c);
	inline void moveCursorLeft();
	inline void moveCursorRight();
	inline void moveCursorUp();
	inline void moveCursorDown();
	inline void doBackspace();
	inline void doDelete();
	void doNewline();
	void redraw();
	int getOffset();
	inline void save();
private:
	void refreshCursor();
	void redrawFrom(int start, int lineNum);
	void redrawLine();

	WINDOW* win;
	TextInstance text;
	int topLineNum=0, leftColNum=0;
	int width, height, startx, starty;
};


inline void EditorWindow::writeChar(char c) {
	text.writeChar(c);
	redrawLine();
	refreshCursor();
}
inline void EditorWindow::moveCursorRight() {
	text.moveRight();
	refreshCursor();
}
inline void EditorWindow::moveCursorLeft() {
	text.moveLeft();
	refreshCursor();
}
inline void EditorWindow::moveCursorUp() {
	text.moveUp();
	refreshCursor();
}
inline void EditorWindow::moveCursorDown() {
	text.moveDown();
	refreshCursor();
}
inline void EditorWindow::doBackspace() {
	text.doBackspace();
	redrawFrom(text.next(text.getLineLoc()), text.getLineNum());
	refreshCursor();
}
inline void EditorWindow::doDelete() {
	text.doDelete();
	redrawFrom(text.next(text.getLineLoc()), text.getLineNum());
	refreshCursor();
}
inline void EditorWindow::save() {
	text.save();
}
#endif //EDITOR_WINDOW_H
