#ifndef TEXT_INSTANCE_H
#define TEXT_INSTANCE_H

#include <vector>
#include <string>
#include <curses.h>

#include "config.h"

class TextInstance
{
public:
	TextInstance();
	TextInstance(const std::string & filename,
			int lineNum=0, int offset=0);
	inline int getLineNum();
	inline int getLineLoc();
	inline const std::vector<char>& getText();
	inline int getBufStart();
	inline int getBufEnd();

	void moveRight();
	void moveLeft();
	void moveUp();
	void moveDown();
	void doNewline();
	inline void doBackspace();
	inline void doDelete();
	inline void moveBufLeft(int times=1);
	inline void moveBufRight(int times=1);
	void writeChar(char c);
	inline int next(int x);
	inline int prev(int x);
	void save();
private:
	void reallocateBuffer();
	void init();

	std::vector<char> text;
	int lineNum=0, lineLoc=0;
	int bufStart=0, bufEnd=0;
	std::string filename="";
};

inline int TextInstance::getLineNum() {
        return lineNum;
}
inline int TextInstance::getLineLoc() {
	return lineLoc;
}
inline void TextInstance::doBackspace() {
	if(bufStart > 1) {
		--bufStart;
		if(text[bufStart] == '\n') {
			for(--lineLoc; text[lineLoc]!='\n'; --lineLoc);
			--lineNum;
		}
	}
}
inline void TextInstance::doDelete() {
	if(bufEnd+1 < (int)text.size()) {
		++bufEnd;
	}
}
inline int TextInstance::getBufStart() {
	return bufStart;
}
inline int TextInstance::getBufEnd() {
	return bufEnd;
}
inline const std::vector<char>& TextInstance::getText() {
	return text;
}
inline void TextInstance::moveBufLeft(int times) {
	for(int x=0; x<times; ++x) {
		text[bufEnd--] = text[--bufStart];
	}
}
inline void TextInstance::moveBufRight(int times) {
	for(int x=0; x<times; ++x) {
		text[bufStart++] = text[++bufEnd];
	}
}
inline int TextInstance::next(int x) {
	++x;
	if(x >= bufStart && x <= bufEnd)
		x = bufEnd+1;
	return x;
}
inline int TextInstance::prev(int x) {
	--x;
	if(x >= bufStart && x <= bufEnd)
		x = bufStart - 1;
	return x;
}
#endif //TEXT_INSTANCE_H
