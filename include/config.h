#ifndef CONFIG_H
#define CONFIG_H

#include <string>

extern const int AUTO_INDENT_SPACES;
extern const int AUTO_INDENT_TABS;
extern const int AUTO_INDENT_NONE;

extern int GAP_BUFFER_SIZE;
extern const int SCROLL_HEIGHT, SCROLL_WIDTH;
extern const int INDENT_SIZE;
extern const int AUTO_INDENT;
extern const std::string INDENT_CHARS;
extern const std::string HORZ_SCROLL_INDICATOR;

#endif //CONFIG_H
