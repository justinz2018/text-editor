#include "EditorWindow.h"
#include "TextInstance.h"

EditorWindow::EditorWindow(int height, int width, int startx,
		int starty, std::string filename)
			: width(width), height(height), startx(startx), starty(starty) {
	win = newwin(this->height, this->width, this->startx, this->starty);
//	wborder(this->win, '|', '|', '-', '-', '+', '+', '+', '+');
	wrefresh(win);
	if(filename.size() > 0) {
		text = TextInstance(filename);
	}
}

EditorWindow::~EditorWindow() {
	delwin(win);
}
void EditorWindow::doNewline() {
	int prevLineLoc = text.getLineLoc();
	int prevLineNum = text.getLineNum();
	text.doNewline();
	redrawFrom(text.next(prevLineLoc), prevLineNum);
	refreshCursor();
}
void EditorWindow::redraw() {
	wclear(win);
	const std::vector<char> vtext = text.getText();
	int st = text.getLineLoc();
	for(int currLine=text.getLineNum();
		st>=1; st=text.prev(st)) {
		if(vtext[text.prev(st)] == '\n') {
			--currLine;
			if(currLine <= topLineNum) break;
		}
	}
	if(vtext[st] == '\n') st = text.next(st);
	std::string currLine = "";
	int lineNum;
	int offset = 0;
	for(lineNum=0; st<(int)vtext.size() && lineNum < height;
			st=text.next(st)) {
		if(vtext[st] == '\n') {
			mvwprintw(win, lineNum, 0,
				"%s\n", currLine.c_str());
			currLine = "";
			++lineNum;
			offset = 0;
			continue;
		}
		if(offset == leftColNum) {
			if(leftColNum > 0) currLine += HORZ_SCROLL_INDICATOR;
			if(leftColNum <= 0 || vtext[st] == '\t') currLine += vtext[st];
		} else if(offset > leftColNum && offset < leftColNum+width) {
			currLine += vtext[st];
		}
		if(vtext[st] == '\t') {
			offset += INDENT_SIZE-(offset%INDENT_SIZE);
		} else ++offset;
	}
	mvwprintw(win, lineNum, 0, "%s\n", currLine.c_str());
	refreshCursor();
	wrefresh(win);
}
void EditorWindow::redrawFrom(int start, int lineNum) {
	const std::vector<char> vtext = text.getText();
	int lineNumIter = lineNum - topLineNum;
	std::string currLine = "";
	int offset = 0;
	for(int x=start;
		x<(int)vtext.size() && lineNumIter < height;
		x=text.next(x)) {
		if(vtext[x] == '\n') {
			mvwprintw(win, lineNumIter, 0,
				"%s\n", currLine.c_str());
			currLine = "";
			++lineNumIter;
			offset = 0;
			continue;
		}
		if(offset == leftColNum) {
			if(leftColNum > 0) currLine += HORZ_SCROLL_INDICATOR;
			if(leftColNum <= 0 || vtext[x] == '\t') currLine += vtext[x];
		} else if(offset > leftColNum && offset < leftColNum+width) {
			currLine += vtext[x];
		}
		if(vtext[x] == '\t') {
			offset += INDENT_SIZE-(offset%INDENT_SIZE);
		} else ++offset;
	}
	mvwprintw(win, lineNumIter, 0, "%s\n", currLine.c_str());
	for(; lineNumIter < height;) {
		mvwprintw(win, ++lineNumIter, 0, "%c", '\n');
	}
	wrefresh(win);
}
void EditorWindow::redrawLine() {
	std::vector<char> vtext = text.getText();
	const int lineNum = text.getLineNum()-topLineNum;

	int x = text.next(text.getLineLoc());

	std::string currLine = "";

	for(int offset=0;; x=text.next(x)) {
		if(x >= (int)vtext.size()) break;
		if(vtext[x]=='\n') break;
		if(offset >= leftColNum+width) break;
		if(offset == leftColNum) {
			if(leftColNum > 0) currLine += HORZ_SCROLL_INDICATOR;
			if(leftColNum <= 0 || vtext[x] == '\t') currLine += vtext[x];
		} else if(offset > leftColNum) {
			currLine += vtext[x];
		}
		if(vtext[x] == '\t') {
			offset += INDENT_SIZE-(offset%INDENT_SIZE);
		} else ++offset;
	}
	mvwprintw(win, lineNum, 0, "%s", currLine.c_str());
	wrefresh(win);
}
int EditorWindow::getOffset() {
	std::vector<char> vtext = text.getText();
	int ret = 0;
	for(int x=text.next(text.getLineLoc()); x<text.getBufStart(); ++x) {
		if(vtext[x] == '\t') {
			ret += INDENT_SIZE-(ret%INDENT_SIZE);
		}
		else ++ret;
	}
	return ret;
}
void EditorWindow::refreshCursor() {
	bool redraw_scr = false;
	if(text.getLineNum() >= topLineNum+height-startx) {
		topLineNum += SCROLL_HEIGHT;
		redraw_scr = true;
	}
	if(text.getLineNum() < topLineNum) {
		topLineNum = std::max(0, topLineNum-SCROLL_HEIGHT);
		redraw_scr = true;
	}
	int offset = getOffset();
	if(offset >= width+leftColNum-starty) {
		leftColNum = std::max(offset-width+SCROLL_WIDTH, leftColNum+SCROLL_WIDTH);

		int tmp = leftColNum%INDENT_SIZE; // Cannot scroll between tabs
		leftColNum += tmp?(INDENT_SIZE-tmp):0;

		redraw_scr = true;
	}
	if(offset < leftColNum+(leftColNum?(int)HORZ_SCROLL_INDICATOR.size()+1:0)) {
		int prev = leftColNum;

		leftColNum -= SCROLL_WIDTH;

		int tmp = leftColNum%INDENT_SIZE; // Cannot scroll between tabs
		leftColNum -= tmp;

		leftColNum = std::max(0, leftColNum);
		redraw_scr = true;
	}

	if(redraw_scr) redraw();
	wmove(win, text.getLineNum()-topLineNum, offset-leftColNum);
	wrefresh(win);
}
