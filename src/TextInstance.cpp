#include "TextInstance.h"
#include "config.h"
#include <fstream>
#include <iterator>
#include <curses.h>


TextInstance::TextInstance() {
	init();
}
void TextInstance::init() {
	text.reserve(GAP_BUFFER_SIZE);
	for(int x=0; x<GAP_BUFFER_SIZE; ++x) {
                text.push_back(0);
        }
        bufEnd = (int)text.size()-1;
        writeChar('\n');
        lineLoc = 0;
        lineNum = 0;
}
TextInstance::TextInstance(const std::string & filename,
			int lineNum, int offset) {
	std::ifstream fin(filename, std::ios::binary);
	this->filename = filename;
	if(!fin.good() || fin.peek() ==
		std::ifstream::traits_type::eof()) {
		init();
		return;
	}
	std::vector<char> vtext((std::istreambuf_iterator<char>(fin)),
				std::istreambuf_iterator<char>());
	int currLineNum = 0;
	int currOffset = 0;
	int lastNewline = 0;
	for(int x=0; x < (int)vtext.size(); ++x) {
		if(currLineNum == lineNum && currOffset == offset) {
			this->lineLoc = lastNewline;
			this->bufStart = (int)(text.size());
			for(int y=0; y<GAP_BUFFER_SIZE; ++y) {
				text.push_back(0);
			}
			this->bufEnd = (int)(text.size())-1;
		}
		text.push_back(vtext[x]);
		if(vtext[x] == '\n') {
			lastNewline = x;
			++currLineNum;
			currOffset = 0;
		} else {
			++currOffset;
		}
	}
	writeChar('\n');
	this->lineNum = lineNum;
}
void TextInstance::moveRight() {
	if(bufEnd+1 >= (int)text.size()) return;
	if(text[bufEnd+1] == '\n') {
		++lineNum;
		lineLoc = bufStart;
	}
	moveBufRight();
}
void TextInstance::moveLeft() {
	if(bufStart == 1) return;
	if(text[bufStart-1] == '\n') {
		--lineNum;
		for(lineLoc=bufStart-2; lineLoc>=0; --lineLoc) {
			if(text[lineLoc] == '\n') {
				break;
			}
		}
	}
	moveBufLeft();
}
void TextInstance::moveUp() {
	if(lineLoc == 0) return;
	int prevLineLoc = lineLoc;
	int shift = bufStart-lineLoc;
	for(--lineLoc; lineLoc>=0 &&
				text[lineLoc] != '\n'; lineLoc=prev(lineLoc));

	--lineNum;
	moveBufLeft(std::max(prevLineLoc-lineLoc, shift));
}
void TextInstance::moveDown() {
	bool passed = false;
	int shift = bufStart-lineLoc-1;
	for(; bufEnd+1 < (int)text.size(); moveBufRight()) {
		if(passed && bufStart-lineLoc-1 == shift) break;
		if(text[bufEnd+1] == '\n') {
			if(passed) break;
			lineLoc = bufStart;
			passed = true;
			++lineNum;
		}
	}
}
void TextInstance::writeChar(char c) {
	text[bufStart++] = c;
	if(bufStart >= bufEnd) {
		reallocateBuffer();
	}
}
void TextInstance::reallocateBuffer() {
	GAP_BUFFER_SIZE *= 2;
	text.resize((int)text.size() + GAP_BUFFER_SIZE);
	for(int x=(int)text.size()-1;; --x) {
		if(x-GAP_BUFFER_SIZE <= bufStart) {
			bufEnd = x;
			break;
		}
		text[x] = text[x-GAP_BUFFER_SIZE];
	}
}
void TextInstance::save() {
	if(filename.size() <= 0) return;
	std::ofstream fout(filename);
	for(int x=next(0); x<(int)text.size(); x=next(x)) {
		fout << text[x];
	}
	fout.close();
}
void TextInstance::doNewline() {
	int num = 0;
	if(AUTO_INDENT != AUTO_INDENT_NONE) {
		for(int x=lineLoc+1; x<bufStart; ++x) {
			if(text[x] == ' ') ++num;
			else if(text[x] == '\t') {
				int tmp = num%INDENT_SIZE;
				num += tmp?(INDENT_SIZE-tmp):INDENT_SIZE;
			}
			else break;
		}
		if(INDENT_CHARS.find(text[bufStart-1]) != std::string::npos) {
                	num += INDENT_SIZE;
		}
	}
	writeChar('\n');
	++lineNum;
	lineLoc = bufStart - 1;
	if(AUTO_INDENT == AUTO_INDENT_SPACES) {
		for(int x=0; x<num; ++x) writeChar(' ');
	} else if(AUTO_INDENT == AUTO_INDENT_TABS) {
		for(int x=0; x<num/INDENT_SIZE; ++x) writeChar('\t');
	}
}
