#include "config.h"

const int AUTO_INDENT_SPACES = 1;
const int AUTO_INDENT_TABS = 2;
const int AUTO_INDENT_NONE = 0;

int GAP_BUFFER_SIZE = 1024;
const int SCROLL_HEIGHT = 5;
const int SCROLL_WIDTH = 10;
const int INDENT_SIZE = 4;
const int AUTO_INDENT = AUTO_INDENT_TABS;
const std::string INDENT_CHARS = "{:";
const std::string HORZ_SCROLL_INDICATOR = "<";
