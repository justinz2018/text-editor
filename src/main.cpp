#include <iostream>
#include <curses.h>

#include "EditorWindow.h"

int main(int argc, char** argv) {
	if(argc < 2) {
		std::cout << "Usage: app filename\n";
		return 0;
	}
	initscr();
	raw();
	keypad(stdscr, TRUE);
	set_escdelay(0);
	set_tabsize(INDENT_SIZE);
	noecho();
	int height, width;
	getmaxyx(stdscr, height, width);
	refresh();
	EditorWindow e(height, width, 0, 0, std::string(argv[1]));
	e.redraw();
	for(int ch=getch();ch!=27;ch=getch()) {
		switch(ch) {
			case KEY_LEFT:
				e.moveCursorLeft();
				break;
			case KEY_DOWN:
				e.moveCursorDown();
				break;
			case KEY_UP:
				e.moveCursorUp();
				break;
			case KEY_RIGHT:
				e.moveCursorRight();
				break;
			case '\r':
			case '\n':
			case KEY_ENTER:
				e.doNewline();
				break;
			case KEY_BACKSPACE:
			case 127:
				e.doBackspace();
				break;
			case KEY_DC:
				e.doDelete();
				break;
			case 19: // CTRL+S
				e.save();
				break;
			default:
				e.writeChar(ch);
				break;
		}
	}

	endwin();
	return 0;
}
